#!/bin/bash

# Constants --------------------------------------------------------------------
DOCKER_CONTAINER_NAME='openvpn-client'

# Utility Functions ------------------------------------------------------------

# Usage: print message
print() {
  printf "$1\n"
}

# Usage: warning warning_message
warning() {
  printf "\e[33m$1\e[m\n"
}

# Usage: error error_message
error() {
  printf "\e[31m$1\e[m\n"
}

# Main Functions ---------------------------------------------------------------

help() {
  print "Usage: `basename $0` [options]"
  print "  -h Display Help"
  print "  -s Stop an existing $DOCKER_CONTAINER_NAME container"
  print "  -r Run an $DOCKER_CONTAINER_NAME container"
  print "  -a Attach to an existing $DOCKER_CONTAINER_NAME container"
}

stop() {
  docker stop "$DOCKER_CONTAINER_NAME"
}

run() {
  docker run \
    --volume `realpath ./configs`:/vpn \
    --cap-add=NET_ADMIN \
    --device /dev/net/tun \
    --dns 1.1.1.1 \
    --publish 80:80 \
    --publish 443:443 \
    --detach \
    --tty \
    --interactive \
    --rm \
    --name "$DOCKER_CONTAINER_NAME" \
    dperson/openvpn-client
}

attach() {
  docker attach "$DOCKER_CONTAINER_NAME"
}

while getopts 'hsra' opt; do
  case "$opt" in
    h)
      help
      ;;
    s)
      stop
      ;;
    r)
      stop
      run
      ;;
    a)
      attach
      ;;
    *)
      help
      exit 128
      ;;
  esac
done

if [[ -z "$1" ]]; then
  help
  exit 128
fi

